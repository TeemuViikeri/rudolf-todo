# rudolf-todo
A preliminary job application task for The Rudolf Oy: create a todo full-stack application.

This application, *SimpleDo*, runs on Heroku. It can be accessed from https://rudolf-todo.herokuapp.com/.

Database for the application is created with MySQL on top of ClearDB MySQL Heroku add-on. Database configurations are taken
from Heroku configuration variables.

Backend includes REST API which has been created with NodeJS framework, Express. Frontend has been created with classic React.

This application was built on top of a school work from last fall which is why frontend code base
uses class implementations and not React Hooks. If I started from scratch, I'd use React Hooks in the future. 

Login can be done via three mock users from `mockUsers.js` file. Preferably, user login information would be in a database and some sort of
encryption and/or authentication should also be used.

Login screen on this application is NOT created with material.io, contrary to the assignment.

Due to personal family life and other time restrictions, I had limited time for this task which is why some things aren't implemented at all or just partly.

