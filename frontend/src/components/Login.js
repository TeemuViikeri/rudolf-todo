import React, { Component } from "react";
import { ReactComponent as LoginPhoto } from './../rudolf-login-todo.svg';

class Login extends Component {
	constructor(props) {
		super();
		this.state = {
			email: "",
			password: "",
		};
	}

  getLoginStyle = () => {
    return {
      display: "flex",
	  flexDirection: "row",
    };
  };

  onLoginInputChange = (e) => {
	if (e.target.type === "email") {
		this.setState({ email: e.target.value });
	}
	
	if (e.target.type === "password") {
		this.setState({ password: e.target.value });
	}
  }

  render() {
    return (
      <div style={{
		display: "flex",
		flexDirection: "row",
		height: "100vh",
	  }}>
		  <LoginPhoto style={{flex: 3}} />

		  <div style={{
			   flex: 2,
			   display: "flex",
			   flexDirection: "column",
			   alignItems: "center",
			   textAlign: "center"
		  }}>
		    <h1 style={{
				letterSpacing: "4px",
				color: "red",
				flex: 1,
				fontSize: "48px",
				fontWeight: "500",
				width: "50%",
				paddingTop: "48px"
			}}>
				The Rudolf
			</h1>
			<div style={{flex: 1, width: "50%"}}>
				<h3 style={{color: "dimgrey", fontWeight: "500"}}>Sign-in!</h3>
				<form style={{marginTop: "32px"}}>
					<input 
						style={{
							width: "100%", 
							padding: "16px", 
							border: "1px solid darkgray",
							borderRadius: "8px"
						}} 
						type="email" 
						placeholder="EMAIL ID"
						onChange={this.onLoginInputChange} />
					<input 
						style={{
							width: "100%", 
							padding: "16px", 
							border: "1px solid darkgray",
							borderRadius: "8px", 
							marginTop: "16px"
						}} 
						type="password" 
						placeholder="PASSWORD"
						onChange={this.onLoginInputChange} />
				</form>
			</div>
			<div style={{
				 width: "50%",
				 flex: 1,
				 cursor: "pointer"
				 }}
				 onClick={
				 this.props.validateLogin.bind(this, this.state.email, this.state.password)}
			>
				<p style={{
					color: "white",
					width: "100%",						
					backgroundColor: "#444d63",
					padding: "16px",
					borderRadius: "8px",
					textAlign: "center",
				}}>
					NEXT
				</p>
			</div>
		 </div>
	  </div>
    );
  }
}

export default Login;
